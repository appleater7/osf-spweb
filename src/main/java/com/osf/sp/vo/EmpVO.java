package com.osf.sp.vo;

import lombok.Data;

@Data
public class EmpVO {

	private Integer empNo;
	private String eName;
	private String job;
	private Integer deptNo;
}
