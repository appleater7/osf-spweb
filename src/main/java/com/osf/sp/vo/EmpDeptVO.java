package com.osf.sp.vo;

import lombok.Data;

@Data
public class EmpDeptVO {

	private Integer empNo;
	private String eName;
	private String job;
	private String mgr;
	private String hireDate;
	private Integer sal;
	private Integer comm;
	private Integer deptNo;
	private String pwd;
	private String lvl;
	private String id;
	private String dName;
	private String loc;
}
