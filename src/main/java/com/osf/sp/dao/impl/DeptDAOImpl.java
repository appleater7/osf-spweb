package com.osf.sp.dao.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.osf.sp.dao.DeptDAO;
import com.osf.sp.vo.DeptVO;

@Repository
public class DeptDAOImpl implements DeptDAO {

	@Autowired
	private SqlSession ss;
	
	@Override
	public List<DeptVO> selectDeptList() {
		return ss.selectList("dept.selectDeptList");
	}

	@Override
	public List<DeptVO> selectDeptList(DeptVO dept) {
		return ss.selectList("dept.selectDeptList",dept);
	}

	@Override
	public Integer updateDeptList(DeptVO dept) {
		return ss.update("dept.updateDept",dept);
	}

	@Override
	public Integer deleteDeptList(Integer deptNo) {
		return ss.delete("dept.deleteDept",deptNo);
	}

	@Override
	public Integer insertDept(DeptVO dept) {
		return ss.insert("dept.java.lang.String",dept);
	}

}
