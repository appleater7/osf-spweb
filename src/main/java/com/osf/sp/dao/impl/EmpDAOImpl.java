package com.osf.sp.dao.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.osf.sp.dao.EmpDAO;
import com.osf.sp.vo.EmpDeptVO;
import com.osf.sp.vo.EmpVO;

@Repository
public class EmpDAOImpl implements EmpDAO {
	
	@Autowired
	private SqlSession ss; 

	@Override
	public List<EmpDeptVO> selectEmpDeptList() {
		return ss.selectList("emp.selectEmpDeptList");
	}

	@Override
	public List<EmpVO> selectEmpList(EmpVO emp) {
		return ss.selectList("emp.selectEmpList", emp);
	}

	@Override
	public Integer updateEmpList(EmpVO emp) {
		return ss.update("emp.updateEmpList", emp);
	}

	@Override
	public Integer deleteEmpList(Integer empNo) {
		return ss.delete("emp.deleteEmpList", empNo);
	}

	@Override
	public Integer insertEmp(EmpVO emp) {
		return ss.insert("emp.insertEmp", emp);
	}
}
