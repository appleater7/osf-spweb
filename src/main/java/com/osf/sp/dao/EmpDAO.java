package com.osf.sp.dao;

import java.util.List;

import com.osf.sp.vo.EmpDeptVO;
import com.osf.sp.vo.EmpVO;

public interface EmpDAO {
	public List<EmpDeptVO> selectEmpDeptList();
	public List<EmpVO> selectEmpList(EmpVO emp);
	public Integer updateEmpList(EmpVO emp);
	public Integer deleteEmpList(Integer empNo);
	public Integer insertEmp(EmpVO emp);
}
