package com.osf.sp.dao;

import java.util.List;

import com.osf.sp.vo.DeptVO;
import com.osf.sp.vo.EmpVO;

public interface DeptDAO {

	public List<DeptVO> selectDeptList();
	public List<DeptVO> selectDeptList(DeptVO dept);
	public Integer updateDeptList(DeptVO dept);
	public Integer deleteDeptList(Integer deptNo);
	public Integer insertDept(DeptVO dept);
}
