package com.osf.sp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.osf.sp.service.DeptService;
import com.osf.sp.vo.DeptVO;
import com.osf.sp.vo.EmpVO;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class DeptController {

	@Autowired
	private DeptService ds;
	
	@RequestMapping(value="/dept/list",method=RequestMethod.GET)
	public String goPage() {
		return "dept_list";
	}
	
	@RequestMapping(value="/depts",method=RequestMethod.GET)
	public @ResponseBody List<DeptVO> selectDeptList(@ModelAttribute DeptVO dept) {
		List<DeptVO> list = ds.selectDeptList();
		log.info("select Dept => {}", list);
		return list;
	}
	
	@RequestMapping(value="/depts",method=RequestMethod.PUT)
	public @ResponseBody Integer doUpdateDept(@RequestBody DeptVO dept) {
		log.info("update dept => {}", dept);
		return ds.updateDeptList(dept);
	}
	
	@RequestMapping(value="/depts/{deptNo}",method=RequestMethod.DELETE)
	public @ResponseBody Integer doDeletedept(@PathVariable("deptNo") Integer deptNo) {
		log.info("delete deptNo => {}", deptNo);
		return ds.deleteDeptList(deptNo);
	}
	
	@RequestMapping(value="/dept",method=RequestMethod.POST)
	public @ResponseBody Integer doInsertdept(@RequestBody DeptVO dept) {
		log.info("insert dept => {}", dept);
		return ds.insertDept(dept);
	}
	
	@RequestMapping(value="/depts",method=RequestMethod.POST)
	public @ResponseBody Integer doInsertdepts(@RequestBody List<DeptVO> depts) {
		log.info("insert depts => {}", depts);
		return ds.insertDepts(depts);
	}
}
