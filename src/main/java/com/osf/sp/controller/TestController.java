package com.osf.sp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class TestController {

	@RequestMapping(value="/testPage",method=RequestMethod.GET)
	public String goTestPage() {
		return "/testPage";
	}
	@RequestMapping(value="/testLogin",method=RequestMethod.GET)
	public String goLoginPage() {
		return "/testLogin";
	}
	
	@RequestMapping(value="/testJoin",method=RequestMethod.GET)
	public String goJoinPage() {
		return "/testJoin";
	}
}
