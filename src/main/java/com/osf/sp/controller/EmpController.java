package com.osf.sp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.osf.sp.service.EmpService;
import com.osf.sp.vo.EmpVO;

import lombok.extern.slf4j.Slf4j;


@Controller
@Slf4j
public class EmpController {
	@Autowired
	private EmpService es;

	@RequestMapping(value="/emp/list",method=RequestMethod.GET)
	public String goPage() {
		log.debug("goPage Start");
		log.info("goPage End");
		return "list";
	}
	
	@RequestMapping(value="/emps",method=RequestMethod.GET)
	public @ResponseBody List<EmpVO> getEmpList(@ModelAttribute EmpVO emp) {
		log.info("param => {}", emp);
		log.info("getEmpList Start");
		List<EmpVO> list = es.getEmpList(emp);
		log.info("empLIst => {}", list);
		log.info("getEmpList End");
		return list;
	}
	
	@RequestMapping(value="/emps",method=RequestMethod.PUT)
	public @ResponseBody Integer doUpdateEmp(@RequestBody EmpVO emp) {
		log.info("update emp => {}", emp);
		return es.updateEmpList(emp);
	}
	
	@RequestMapping(value="/emps/{empNo}",method=RequestMethod.DELETE)
	public @ResponseBody Integer doDeleteEmp(@PathVariable("empNo") Integer empNo) {
		log.info("delete empNo => {}", empNo);
		return es.deleteEmpList(empNo);
	}
	
	@RequestMapping(value="/emp",method=RequestMethod.POST)
	public @ResponseBody Integer doInsertEmp(@RequestBody EmpVO emp) {
		log.info("insert emp => {}", emp);
		return es.insertEmp(emp);
	}
	
	@RequestMapping(value="/emps",method=RequestMethod.POST)
	public @ResponseBody Integer doInsertEmps(@RequestBody List<EmpVO> emps) {
		log.info("insert emps => {}", emps);
		return es.insertEmps(emps);
	}
}
