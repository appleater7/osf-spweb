package com.osf.sp.service;

import java.util.List;

import com.osf.sp.vo.DeptVO;
import com.osf.sp.vo.EmpVO;

public interface DeptService {
	public List<DeptVO> selectDeptList();
	public List<DeptVO> getDeptList(DeptVO dept);
	public Integer updateDeptList(DeptVO dept);
	public Integer deleteDeptList(Integer deptNo);
	public Integer insertDept(DeptVO dept);
	public Integer insertDepts(List<DeptVO> depts);
}
