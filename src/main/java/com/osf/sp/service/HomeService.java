package com.osf.sp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;

import com.osf.sp.vo.EmpVO;

public class HomeService {

	@Autowired
	private SqlSession ss;
	
	public List<String> getStrList() {
		List<String> strList = new ArrayList<String>();
		strList.add("str1");
		strList.add("str2");
		return strList;
	}
	
	public List<EmpVO> getEmpList() {
		return ss.selectList("emp.selectEmpList");
	}
	public List<Map<String, Object>> EmpListJoinDept() {
		return ss.selectList("emp.selectEmpListJoinDept");
	}
}