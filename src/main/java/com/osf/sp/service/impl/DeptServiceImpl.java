package com.osf.sp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.osf.sp.dao.DeptDAO;
import com.osf.sp.service.DeptService;
import com.osf.sp.vo.DeptVO;
import com.osf.sp.vo.EmpVO;

@Service
public class DeptServiceImpl implements DeptService {

	@Autowired
	private DeptDAO ddao;
	
	@Override
	public List<DeptVO> selectDeptList() {
		return ddao.selectDeptList();
	}

	@Override
	public List<DeptVO> getDeptList(DeptVO dept) {
		return ddao.selectDeptList(dept);
	}

	@Override
	public Integer updateDeptList(DeptVO dept) {
		return ddao.updateDeptList(dept);
	}

	@Override
	public Integer deleteDeptList(Integer deptNo) {
		return ddao.deleteDeptList(deptNo);
	}

	@Override
	public Integer insertDept(DeptVO dept) {
		return ddao.insertDept(dept);
	}

	@Override
	public Integer insertDepts(List<DeptVO> depts) {
		int cnt = 0;
		for (DeptVO dept:depts) {
			cnt += ddao.insertDept(dept);
		}
		return cnt;
	}

}
