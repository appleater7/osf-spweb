package com.osf.sp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.osf.sp.dao.EmpDAO;
import com.osf.sp.service.EmpService;
import com.osf.sp.vo.EmpDeptVO;
import com.osf.sp.vo.EmpVO;

@Service
public class EmpServiceImpl implements EmpService {

	@Autowired
	private EmpDAO edao;

	@Override
	public List<EmpVO> getEmpList(EmpVO emp) {
		return edao.selectEmpList(emp);
	}

	@Override
	public List<EmpDeptVO> getEmpDeptList() {
		return edao.selectEmpDeptList();
	}

	@Override
	public Integer updateEmpList(EmpVO emp) {
		return edao.updateEmpList(emp);
	}
	
	@Override
	public Integer deleteEmpList(Integer empNo) {
		return edao.deleteEmpList(empNo);
	}

	@Override
	public Integer insertEmp(EmpVO emp) {
		return edao.insertEmp(emp);
	}

	@Override
	public Integer insertEmps(List<EmpVO> emps) {
		int cnt = 0;
		for (EmpVO emp:emps) {
			cnt += edao.insertEmp(emp);
		}
		return cnt;
	}
}
