package com.osf.sp.service;

import java.util.List;

import com.osf.sp.vo.EmpDeptVO;
import com.osf.sp.vo.EmpVO;

public interface EmpService {

	public List<EmpDeptVO> getEmpDeptList();
	public List<EmpVO> getEmpList(EmpVO emp);
	public Integer updateEmpList(EmpVO emp);
	public Integer deleteEmpList(Integer empNo);
	public Integer insertEmp(EmpVO emp);
	public Integer insertEmps(List<EmpVO> emps);
}
