package com.osf.sp.advice;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Aspect
@Slf4j
public class LogAdvice {
	private long start;
	private long end;

	@Before("execution(* com.osf.sp.controller.*.*(..))") // controller.*(클래스모두).*(..)(메소드모두,(..)파라미터 0개이상
	public void beforeLog(JoinPoint jp) {
		start = System.currentTimeMillis();
		log.debug("컨트롤러 앞에 난 시작");
	}
	@After("execution(* com.osf.sp.controller.*.*(..))") // controller.*(클래스모두).*(..)(메소드모두,(..)파라미터 0개이상
	public void afterLog(JoinPoint jp) {
		end = System.currentTimeMillis();
		log.debug("컨트롤러 뒤에 난 시작");
		log.debug("execute time : " + (end - start)/1000.0);
	}
}
