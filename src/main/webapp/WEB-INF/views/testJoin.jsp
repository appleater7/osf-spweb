<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<br><br><br><br>
	<h1>Sign-up</h1>

	이름<input id="ui_name" type="text" placeholder="name 을 입력하세요.">
	<br>
	나이<input id="ui_age" type="number" placeholder="age 을 입력하세요.">
	<br>
	아디<input id="ui_id" type="text" placeholder="id 를 입력하세요.">
	<br>
	비번<input id="ui_pwd" type="password" placeholder="password 를 입력하세요.">
	<br>
	<button id="button" onclick="login()">회원가입</button>
	<script>
		var uiName = document.querySelector('#ui_name');
		var uiAge = document.querySelector('#ui_age');
		var uiId = document.querySelector('#ui_id');
		var uiPwd = document.querySelector('#ui_pwd');
	
		function HangulCheck(obj) {
			var chars = obj.split("");
			var deny_char = /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/;
			for(cha of chars){
				if (!deny_char.test(cha)){
					alert('한글만 입력하세요.');
					return false;
				}		
			}
			if (obj.length>6){
				alert('이름 길이가 6자를 초과하였습니다.');
				return false;
			}
			return true;
		}
		function AlphaCheck(obj) {
			var chars = obj.split("");
			var deny_char = /^[A-Za-z0-9]+$/;
			for(cha of chars){
				if (!deny_char.test(cha)){
					alert('영문과 숫자만 입력하세요.');
					return false;
				}				
			}
			if (obj.length>30){
				alert('길이가 30자를 초과하였습니다.');
				return false;
			}
			return true;
		}
		function AgeCheck(obj) {
			if (!(obj>=0 && obj<1000)){
				alert('나이를 정확히 입력해주세요.');
				return false;
			}
			return true;
		}
		function login() {
			if (!((uiId.value != 0)&&(uiPwd.value != 0)&&
				(uiAge.value != 0)&&(uiName.value != 0))){
				alert('모든 값을 입력하여주세요.');
			}else {
				if (HangulCheck(uiName.value)&&AlphaCheck(uiId.value)&&
						AlphaCheck(uiPwd.value)&&AgeCheck(uiAge.value)){
					alert('정보-정상입력-회원가입진행');
					return true;					
				}else {
					return false;
				}				
			}
		}
	</script>
</body>
</html>