<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1>Login</h1>
<input id="id" type="text" placeholder="id 를 입력하세요."><br>
<input id="pwd" type="password" placeholder="password 를 입력하세요."><br>
<button id="button" onclick="login()">로그인</button>
<script>
	var id = document.querySelector('#id');
	var pwd = document.querySelector('#pwd');
	function login() {
		if(id.value.length<5){
			alert('아이디를 5글자 이상 입력하세요.');
		}else {
			if(pwd.value.length<8){
				alert('비밀번호를 8글자 이상 입력하세요.');	
			}else {
				alert('정상적인 입력값입니다.');
			}			
		}		
	}
	
</script>
</body>
</html>