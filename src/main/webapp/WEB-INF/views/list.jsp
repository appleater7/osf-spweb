<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="rPath" value="${pageContext.request.contextPath}" />
<c:set var="dPath" value="${rPath}/resources/dhtmlx" />
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${rPath}/resources/css/main.css" />
<link rel="stylesheet" href="${dPath}/codebase/dhtmlx.css" />
<script src="${dPath}/codebase/dhtmlx.js"></script>
<script src="${rPath}/resources/js/AjaxUtils.js"></script>
</head>
<body>
	<div id="empGrid" style="width: 400px; height: 300px;"></div>
	사원번호 :
	<input type="number" id="empNo" class="emp"><br>
	사원이름 :
	<input type="text" id="eName" class="emp"><br>
	직급 :
	<input type="text" id="job" class="emp"><br>
	<button onclick="searchEmp()">검색</button>
	<button onclick="saveEmp()">저장</button>
	<button onclick="saveEmps()">저장s</button>
	<div id="deptGrid" style="width: 400px; height: 300px;"></div>
	부서번호 :
	<input type="number" id="deptNo" class="dept"><br>
	부서명 :
	<input type="text" id="dName" class="dept"><br>
	지역 :
	<input type="text" id="loc" class="dept"><br>
	<button onclick="searchEmp()">검색</button>
	<button onclick="saveEmp()">저장</button>
	<button onclick="saveEmps()">저장s</button>

	<script>
		var rIds = [];
		var menuForm = [{id:'add',text:'추가'},{id:'del',text:'삭제'}];
		var dxMenu = new dhtmlXMenuObject();
		dxMenu.setIconsPath('${dPath}/codebase/imgs');
		dxMenu.renderAsContextMenu();
		dxMenu.attachEvent('onClick', function(id,type) {
			if (id == 'add') {
				var rId = grid.uid();
				grid.addRow(rId,{},grid.getRowsNum());
				grid.setRowAttribute(rId,'status','n');
				rIds.push(rId);
			}else if (id == 'del') {
				var rId = grid.getSelectedRowId();
				if (rId == null) {
					alert('삭제할 row를 선택 해주세요.');
					return;
				}
				var ind = grid.getColIndexById('empNo');
				var empNo = grid.cells(rId,ind).getValue();
				alert(empNo);
				var xhr = new XMLHttpRequest();
				xhr.open('delete', '${rPath}/emps/' + empNo,false);
				xhr.setRequestHeader('Content-type','application/json');
				xhr.onreadystatechange = function() {
					if (xhr.readyState == xhr.DONE) {
						if (xhr.status == 200) {
							if (xhr.response==='1') {
								alert('삭제완료');
								
							}else {
								alert('삭제실패');
							}
						}
					}
				}
				xhr.send();
			}
		})
		dxMenu.loadStruct(menuForm);
		
		var deptGrid = new dhtmlXGridObject('deptGrid');
		deptGrid.setImagePath('${dPath}/codebase/imgs/');
		deptGrid.setHeader('부서번호,부서명,지역', null, [ "text-align:center;",
				"text-align:center;", "text-align:center;" ]);
		deptGrid.setColumnIds('deptNo,dname,loc');
		deptGrid.setColAlign('center,center,center');
		deptGrid.setColTypes('ro,ed,ed'); //ro:readOnly, ed:editAble
		deptGrid.setColSorting('int,str,str');
		deptGrid.enableContextMenu(dxMenu);
		deptGrid.init();
		
		var empGrid = new dhtmlXGridObject('empGrid');
		empGrid.setImagePath('${dPath}/codebase/imgs/');
		empGrid.setHeader('사원번호,사원이름,직급', null, [ "text-align:center;",
				"text-align:center;", "text-align:center;" ]);
		empGrid.setColumnIds('empNo,ename,job');
		empGrid.setColAlign('center,center,center,center');
		empGrid.setColTypes('ro,ed,ed'); //ro:readOnly, ed:editAble
		empGrid.setColSorting('int,str,str');
		empGrid.enableContextMenu(dxMenu);
		empGrid.init();
		
		var conf = {
				url:'/depts',
				suc:function(res) {
					deptGrid.parse(res,'js');
				}
			}
		var au = new AjaxUtil(conf);
		au.send();
		
		//var data = JSON.parse('${empList}');
		//grid.parse(data,'js'); // 1.csv 2.json 3.js
		empGrid.attachEvent('onRowSelect', function(id, ind) {
			var empNO = this.cells(id, 0).getValue();
			var ename = this.cells(id, 1).getValue();
			var job = this.cells(id, 2).getValue();
			document.querySelector('#empNo').value = empNO;
			document.querySelector('#eName').value = ename;
			document.querySelector('#job').value = job;
		});
		
		empGrid.attachEvent('onEditCell',function(stage, rId, ind, nV, oV) {
			var rStatus = empGrid.getRowAttribute(rId,'status');
			if (rStatus=='n') {
				return true;
			}
			if (stage == 2) {
				if (nV != oV) {
					var data = empGrid.getRowData(rId);
					data = JSON.stringify(data);
					var xhr = new XMLHttpRequest();
					xhr.open('PUT', '${rPath}/emps',false);
					xhr.setRequestHeader('Content-type','application/json');
					var res = false;
					xhr.onreadystatechange = function() {
						if (xhr.readyState == xhr.DONE) {
							if (xhr.status == 200) {
								alert('수정 완료');
								return true;
							}
						}
					}
					xhr.send(data);
					return res;
				}
			}
		});
		function makeParam() {
			var inputs = document.querySelectorAll('input.emp');
			var param = '';
			for (var input of inputs) {
				if (input.value.trim() != '') {
					param += input.id + '=' + input.value + '&';	
				}				
			}
			return param;
		}
		function searchEmp() {
			var url = '${rPath}/emps?' + makeParam();
			var xhr = new XMLHttpRequest();
			xhr.open('GET', url);
			xhr.onreadystatechange = function() {
				if (xhr.readyState == xhr.DONE) {
					if (xhr.status == 200) {
						empGrid.clearAll();
						empGrid.parse(xhr.response, 'js');
					}
				}
			}
			xhr.send();
		}
		function saveEmp() {
			for (var rId of rIds) {
				var data = JSON.stringify(grid.getRowData(rId));
				var xhr = new XMLHttpRequest();
				xhr.open('POST','${rPath}/emp', false);
				xhr.setRequestHeader('Content-type','application/json');
				xhr.onreadystatechange = function() {
					if (xhr.readyState == xhr.DONE) {
						if (xhr.status == 200) {
							if (xhr.response==='1') {
								alert('등록 완료');
							}else {
								alert('등록 실패');
							}
						}
					}
				}
				xhr.send(data);
			}
		}
		function saveEmps() {
			var emps = [];
			for (var rId of rIds) {
				emps.push(grid.getRowData(rId));			
			}
			var xhr = new XMLHttpRequest();
			xhr.open('POST','${rPath}/emps', false);
			xhr.setRequestHeader('Content-type','application/json');
			xhr.onreadystatechange = function() {
				if (xhr.readyState == xhr.DONE) {
					if (xhr.status == 200) {
						if (xhr.response==rIds.length) {
							alert('등록s 완료');
						}else {
							alert('등록s 실패');
						}
					}
				}
			}
			xhr.send(JSON.stringify(emps));
		}
		searchEmp();
	</script>
</body>
</html>