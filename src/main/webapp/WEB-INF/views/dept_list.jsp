<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="rPath" value="${pageContext.request.contextPath}" />
<c:set var="dPath" value="${rPath}/resources/dhtmlx" />
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${rPath}/resources/css/main.css" />
<link rel="stylesheet" href="${dPath}/codebase/dhtmlx.css" />
<script src="${dPath}/codebase/dhtmlx.js"></script>
</head>
<body>
	<div id="gridbox" style="width: 300px; height: 200px;"></div>
	부서번호 : 
	<input type="number" id="deptNo"><br>
	부서명   : 
	<input type="text" id="dName"><br>
	지역     : 
	<input type="text" id="loc"><br>
	<button onclick="searchDept()">검색</button>
	<button onclick="saveDept()">저장</button>
	<button onclick="saveDepts()">저장s</button>
	<script>
		var grid = new dhtmlXGridObject('gridbox');
		grid.setImagePath('${dPath}/codebase/imgs/');
		grid.setHeader('선택,부서번호,부서명,지역', null, [ 'text-align:center',
						'text-align:center', 'text-align:center','text-align:center' ]);
		grid.setColumnIds('check,deptNo,dname,loc');
		grid.setColAlign('center,center,center,center');
		grid.setColTypes('ch,ed,ed,ed');
		grid.setColSorting('int,int,str,str');
		grid.init();

		grid.attachEvent('onRowSelect', function(id, ind) {
			var deptNo = this.cells(id, 1).getValue();
			var dName = this.cells(id, 2).getValue();
			var loc = this.cells(id, 3).getValue();
			document.querySelector('#deptNo').value = deptNo;
			document.querySelector('#dName').value = dName;
			document.querySelector('#loc').value = loc;
		});
		
		function makeParam() {
			var inputs = document.querySelectorAll('input');
			var param = '';
			for (var input of inputs) {
				if (input.value.trim() != '') {
					param += input.id + '=' + input.value + '&';
				}
			}
			return param;
		}
		
		function searchDept() {
			var xhr = new XMLHttpRequest();
			var url = '${rPath}/depts/?' + makeParam();
			xhr.open('GET', url);
			xhr.onreadystatechange = function() {
				if (xhr.readyState == xhr.DONE) {
					if (xhr.status == 200) {
						console.log(xhr.response);
						grid.parse(xhr.response, 'js');
					}
				}
			}
			xhr.send();
		}
		searchDept();
	</script>
</body>
</html>