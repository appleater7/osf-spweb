<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="rPath" value="${pageContext.request.contextPath}"/>
<c:set var="dPath" value="${rPath}/resources/dhtmlx" />
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${rPath}/resources/css/main.css" />
<link rel="stylesheet" href="${dPath}/codebase/dhtmlx.css" />
<script src="${dPath}/codebase/dhtmlx.js"></script>
<script src="${rPath}/resources/js/AjaxUtils.js"></script>
</head>
<body>
<div id="topGrid" style="width:500px; height:300px"></div>
<div id="middleGrid" style="width:500px; height:300px"></div>
<script>
var topGrid = new dhtmlXGridObject('topGrid');
topGrid.setHeader('부서번호,부서명,지역',null,["text-align:center","text-align:center","text-align:center"]);
topGrid.setColumnIds('deptNo,dname,loc');
topGrid.setColTypes('ro,ed,ed');
topGrid.setColAlign('center,center,center');
topGrid.init();
topGrid.attachEvent('onRowSelect',function(rId,ind){
	var data = topGrid.getRowData(rId);
	var au = new AjaxUtil({url:'/emps?deptNo='+data.deptNo,suc:callback2});
	au.send();
})

var middleGrid = new dhtmlXGridObject('middleGrid');
middleGrid.setHeader('사원번호,이름,부서번호,직급',null,["text-align:center","text-align:center","text-align:center","text-align:center"]);
middleGrid.setColumnIds('empNo,ename,deptNo,job');
middleGrid.setColAlign('center,center,center,center');
middleGrid.setColTypes('ro,ed,ro,ed');
middleGrid.init();

function callback(res) {
	topGrid.parse(res,'js');
	topGrid.selectRow(0);
	var rowId = topGrid.getSelectedRowId();
	var data = topGrid.getRowData(rowId);
	var au = new AjaxUtil({url:'/emps?deptNo='+data.deptNo,suc:callback2});
	au.send();
}
function callback2(res) {
	middleGrid.clearAll();
	middleGrid.parse(res,'js');
}
var au = new AjaxUtil({url:'/depts',suc:callback});
au.send();
</script>
</body>
</html>